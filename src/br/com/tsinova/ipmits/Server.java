package br.com.tsinova.ipmits;

import java.util.List;
import org.json.JSONObject;

public class Server {

    private static final String REQUIRED_ATTRIBUTES[] = new String[]{"chassis_type"};
    public static final String PREFIX = "server_";
    private static final String TYPE = "Server";    
    
    /**
     * Coleta as informações do servidor
     *
     * @param output
     * @param host
     * @return
     * @throws Exception
     */
    public static JSONObject getServerByFru(List<List<String>> output, String host) throws Exception {

        for (List<String> deviceInfo : output) {

            boolean deviceIsServer;

            try {
                deviceIsServer = Util.contaisOneKeysInDeviceInfo(deviceInfo, REQUIRED_ATTRIBUTES);
            } catch (Exception ex) {
                continue;
            }

            if (!deviceIsServer) {
                continue;
            }

            try {
                JSONObject jsonServerInfo = Util.convertListOutputToJSONObject(deviceInfo, PREFIX);
                jsonServerInfo.put("type", TYPE);
                jsonServerInfo.put("id_doc", getIdDoc(jsonServerInfo, 
                        new String[]{"serial_number", "product_serial", "chassis_serial"}, host, 
                        "fru_device_description"));                                
                return jsonServerInfo;

            } catch (Exception ex) {
            }

        }

        return null;

    }

    private static String getIdDoc(JSONObject json, String keys[], String host, String k) throws Exception {
        for (String key : keys) {
            if (!json.has(PREFIX + key)) {
                continue;
            }
            return "server_" + json.getString(PREFIX + key);
        }
        return "server_" + host + "_" + json.getString(PREFIX + k);
    }
    
    
}
