package br.com.tsinova.ipmits;

import org.json.JSONException;
import org.json.JSONObject;

public class Host {
    
    private int interval;
    private String ip;
    private String user;
    private String password;
        
    public Host(JSONObject json) throws JSONException {
        interval = json.getInt("interval");
        ip = json.getString("ip");
        user = json.getString("user");
        password = json.getString("password");
    }
    
    public Host() {
    }    

    
    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }    
    
}
