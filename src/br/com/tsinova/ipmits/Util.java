package br.com.tsinova.ipmits;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class Util {

    public static String getDatetimeNowForElasticsearch() {
        return Instant.now().toString();
    }

    public static boolean contaisOneKeysInDeviceInfo(List<String> deviceInfo, String[] keys) throws Exception {

        if (deviceInfo == null || deviceInfo.isEmpty()) {
            return false;
        }

        for (int i = 1; i < deviceInfo.size(); i++) {

            String line = deviceInfo.get(i);

            String keyLine;

            try {
                keyLine = convertLineInKey(line);
            } catch (Exception ex) {
                continue;
            }

            boolean exists = false;

            for (String key : keys) {
                if (key.equalsIgnoreCase(keyLine)) {
                    exists = true;
                    break;
                }
            }

            if (exists) {
                return true;
            }

        }

        return false;

    }

    public static boolean contaisOneValueInDeviceInfo(List<String> deviceInfo, String[] values) throws Exception {

        if (deviceInfo == null || deviceInfo.isEmpty()) {
            return false;
        }

        for (int i = 1; i < deviceInfo.size(); i++) {

            String line = deviceInfo.get(i);

            String valueLine;

            try {
                valueLine = convertLineInValue(line);
            } catch (Exception ex) {
                continue;
            }

            boolean exists = false;

            for (String value : values) {
                if (valueLine.matches(value)) {
                    exists = true;
                    break;
                }
            }

            if (exists) {
                return true;
            }

        }

        return false;

    }

    public static List<String> convertInputStreamInListString(InputStream inputStream) throws Exception {
        List<String> output = new ArrayList<>();
        try (BufferedReader input = new BufferedReader(new InputStreamReader(inputStream))) {
            String lineOut;
            while ((lineOut = input.readLine()) != null) {
                output.add(lineOut);
            }
        }
        return output;

    }

    public static JSONObject convertListOutputToJSONObject(List<String> output, String prefix) throws Exception {
        JSONObject json = new JSONObject();
        for (String line : output) {
            String key = prefix + Util.convertLineInKey(line);
            String value = Util.convertLineInValue(line);
            json.put(key, value);
        }
        return json;
    }

    public static String convertLineInKey(String line) throws Exception {

        if (line == null) {
            throw new Exception("value invalid! value cannot be null.");
        }

        if (line.trim().isEmpty() || !line.trim().contains(":")) {
            throw new Exception("value invalid! value does not have a colon (:)");
        }

        String key = line.split(":")[0];
        key = key.trim();
        key = key.toLowerCase();
        key = key.replace(" ", "_").replace("-", "_").replace("/", "_").replace(".", "_");

        return key.trim();

    }

    public static String convertLineInValue(String line) throws Exception {

        if (line == null) {
            throw new Exception("value invalid! value cannot be null.");
        }

        if (line.trim().isEmpty() || !line.trim().contains(":")) {
            throw new Exception("value invalid! value does not have a colon (:)");
        }

        String value = line.split(":")[1];
        value = value.trim();

        return value.trim();

    }

    private static JSONObject getJson(JSONObject jsonStart, String key) throws Exception {

        String keys[] = key.split("\\.");

        if (keys.length < 2) {
            return jsonStart;
        }

        int i = 0;
        JSONObject json = null;

        while (i < keys.length - 1) {
            if (json == null) {
                json = jsonStart.getJSONObject(keys[i]);
            } else {
                json = json.getJSONObject(keys[i]);
            }
            i++;
        }

        return json;

    }

    public static JSONArray getJSONArrayEmpty() {
        return new JSONArray();
    }

    private static String getLastKey(String key) {
        String keys[] = key.split("\\.");
        return keys[keys.length - 1];
    }

    public static Object getValueJson(Type type, JSONObject jsonStart, String keys, Object valueDefault) throws Exception {

        if (jsonStart == null) {
            return valueDefault;
        }

        if (keys == null || keys.isEmpty()) {
            return valueDefault;
        }

        // separa em vários kays/indexs
        String listKeys[] = keys.split(";");

        boolean flag = false;
        String keySearch = "";
        JSONObject json = null;

        // percore todos os indexs
        for (String key : listKeys) {

            key = key.trim();

            try {
                json = getJson(jsonStart, key);
                keySearch = getLastKey(key);
            } catch (Exception ex) {
            }

            if (json != null && json.has(keySearch)) {
                flag = true;
                break;
            }

        }

        if (!flag) {
            return valueDefault;
        }

        Object value = json.get(keySearch);

        if (value == null) {
            return valueDefault;
        }

        if (type == Type.TEXT) {
            if (value.toString().equals("null")) {
                return valueDefault;
            }
            return value.toString();
        }

        if (type == Type.INTEGER) {
            return (int) value;
        }

        if (type == Type.DOUBLE) {
            if (value instanceof Integer) {
                return (int) value;
            }
            return (double) value;
        }

        if (type == Type.LONG) {
            return (long) value;
        }

        if (type == Type.ARRAY_JSON) {
            return (JSONArray) value;
        }

        throw new Exception("Invalid data type!");

    }

    public static String convertObjectToString(Object value, String valueDefault) {
        if (value == null) {
            return valueDefault;
        }
        return (String) value;
    }

    public static Integer convertObjectToInteger(Object value, Integer valueDefault) {
        if (value == null) {
            return valueDefault;
        }
        return (int) value;
    }

    public static JSONArray convertObjectToJSONArray(Object value) {
        JSONArray jSONArray = new JSONArray();
        if (value == null) {
            return jSONArray;
        }
        jSONArray = (JSONArray) value;
        return jSONArray;
    }

}
