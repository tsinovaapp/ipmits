package br.com.tsinova.ipmits;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;

public class Chassis {

    private static final String PREFIX = "chassistatus_";
    private static final String TYPE = "Chassis Status";

    /**
     * Retorna o status do chassi
     *
     * @param host
     * @param user
     * @param password
     * @param passwordSudo
     * @param keyServerSerial
     * @param valueServerSerial
     * @return
     * @throws Exception
     */
    public static JSONObject getChassisStatus(String host, String user,
            String password, String passwordSudo, String keyServerSerial, String valueServerSerial) throws Exception {

        JSONObject json = new JSONObject();

        String[] cmd = {"/bin/bash", "-c", "echo " + passwordSudo + "| sudo ipmitool -I lanplus -H " + host + " -U " + user + " -P '" + password + "' chassis status "};

        Process p = Runtime.getRuntime().exec(cmd);

        if (!p.waitFor(10, TimeUnit.SECONDS)) {
            p.destroy();
            return json;
        }

        List<String> output = Util.convertInputStreamInListString(p.getInputStream());

        p.destroy();

        json = Util.convertListOutputToJSONObject(output, PREFIX);
        json.put("type", TYPE);
        json.put(keyServerSerial, valueServerSerial);

        
        return json;

    }

}
