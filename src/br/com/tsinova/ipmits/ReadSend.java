package br.com.tsinova.ipmits;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ReadSend extends Thread {

    private final Host host;
    private final List<Output> listOutput;
    private boolean run;
    private final Beat beat;

    public ReadSend(Host host, List<Output> listOutput, Beat beat) {
        this.host = host;
        this.listOutput = listOutput;
        this.run = true;
        this.beat = beat;
    }

    public void close() {
        this.run = false;
    }

    private JSONObject getJson(JSONObject jsonValuesDefault, JSONObject jsonValues) throws JSONException {

        JSONObject json = new JSONObject();

        Iterator it = jsonValuesDefault.keys();
        while (it.hasNext()) {
            String key = it.next().toString();
            json.put(key, jsonValuesDefault.get(key));
        }

        it = jsonValues.keys();
        while (it.hasNext()) {
            String key = it.next().toString();
            json.put(key, jsonValues.get(key));
        }

        return json;
    }

    @Override
    public void run() {

        while (run) {

            try {

                System.out.println("Searching information " + host.getIp());

                List<List<String>> fru = Fru.getFruByIPMItool(host.getIp(), host.getUser(), host.getPassword(), beat.getPasswordSudo());

                JSONObject server = Server.getServerByFru(fru, host.getIp());

                String keyServerSerial = Server.PREFIX + "product_serial";
                String valueServerSerial = Util.convertObjectToString(Util.getValueJson(Type.TEXT,
                        server,
                        keyServerSerial,
                        ""),
                        "");

                if (valueServerSerial == null || valueServerSerial.isEmpty()) {
                    System.out.println("Error: Server does not have serial number.");
                    try {
                        Thread.sleep(host.getInterval() * 1000);
                    } catch (InterruptedException ex) {
                    }
                    continue;
                }

                JSONArray cpus = CPU.getCPUsByFru(fru, host.getIp(), keyServerSerial, valueServerSerial);                
                JSONArray memorys = Memory.getMemorysByFru(fru, host.getIp(), keyServerSerial, valueServerSerial);                                
                JSONObject chassiStatus = Chassis.getChassisStatus(host.getIp(), host.getUser(), 
                        host.getPassword(), beat.getPasswordSudo(), keyServerSerial, valueServerSerial);                                                
                JSONArray warrantys = Warranty.getWarrantyBySerialNumberServer(server, host.getIp(), 
                        keyServerSerial, valueServerSerial);
                JSONArray sensors = Sensor.getSensors(host.getIp(), host.getUser(), 
                        host.getPassword(), beat.getPasswordSudo(), keyServerSerial, valueServerSerial);

                System.out.println("-----------------------------------------------------");

                System.out.println(memorys.toString());
                System.out.println("\n");
                System.out.println(cpus.toString());
                System.out.println("\n");
                System.out.println(server.toString());
                System.out.println("\n");
                System.out.println(chassiStatus.toString());
                System.out.println("\n");
                System.out.println(warrantys.toString());
                System.out.println("\n");
                System.out.println(sensors.toString());

                System.out.println("-----------------------------------------------------");

                System.out.println("Search performed on " + host.getIp() + " successfully");

                // Seta outros atributos, atributos que qualquer documento tem
                JSONObject jsonDefault = new JSONObject();

                try {

                    jsonDefault.put("timestamp", Util.getDatetimeNowForElasticsearch());

                    JSONObject device = new JSONObject();
                    device.put("ip", host.getIp());
                    jsonDefault.put("device", device);

                    JSONObject metadataJSON = new JSONObject();
                    metadataJSON.put("beat", beat.getName());
                    metadataJSON.put("version", beat.getVersion());
                    jsonDefault.put("@metadata", metadataJSON);

                    JSONObject beatJSON = new JSONObject();
                    beatJSON.put("name", beat.getName());
                    beatJSON.put("version", beat.getVersion());
                    jsonDefault.put("beat", beatJSON);

                    jsonDefault.put("tags", beat.getTags());

                } catch (JSONException ex) {
                }

                System.out.println("Sending data ... ");

                for (Output out : listOutput) {

                    // encaminha dados do servidor (sobre)
                    try (Socket socket = new Socket(out.host, out.port);
                            DataOutputStream os = new DataOutputStream(
                                    new BufferedOutputStream(socket.getOutputStream()))) {
                        os.writeBytes(getJson(jsonDefault, server).toString());
                        os.flush();
                    }

                    // encaminha dados das cpus
                    for (int i = 0; i < cpus.length(); i++) {
                        try (Socket socket = new Socket(out.host, out.port);
                                DataOutputStream os = new DataOutputStream(
                                        new BufferedOutputStream(socket.getOutputStream()))) {
                            os.writeBytes(getJson(jsonDefault, cpus.getJSONObject(i)).toString());
                            os.flush();
                        }
                    }

                    // encaminha dados das memórias
                    for (int i = 0; i < memorys.length(); i++) {
                        try (Socket socket = new Socket(out.host, out.port);
                                DataOutputStream os = new DataOutputStream(
                                        new BufferedOutputStream(socket.getOutputStream()))) {
                            os.writeBytes(getJson(jsonDefault, memorys.getJSONObject(i)).toString());
                            os.flush();
                        }
                    }

                    // encaminha status do chassi
                    try (Socket socket = new Socket(out.host, out.port);
                            DataOutputStream os = new DataOutputStream(
                                    new BufferedOutputStream(socket.getOutputStream()))) {
                        os.writeBytes(getJson(jsonDefault, chassiStatus).toString());
                        os.flush();
                    }

                    // encaminha as garantias
                    for (int i = 0; i < warrantys.length(); i++) {
                        try (Socket socket = new Socket(out.host, out.port);
                                DataOutputStream os = new DataOutputStream(
                                        new BufferedOutputStream(socket.getOutputStream()))) {
                            os.writeBytes(getJson(jsonDefault, warrantys.getJSONObject(i)).toString());
                            os.flush();
                        }
                    }

                    // encaminha dados dos sensores
                    for (int i = 0; i < sensors.length(); i++) {
                        try (Socket socket = new Socket(out.host, out.port);
                                DataOutputStream os = new DataOutputStream(
                                        new BufferedOutputStream(socket.getOutputStream()))) {
                            os.writeBytes(getJson(jsonDefault, sensors.getJSONObject(i)).toString());
                            os.flush();
                        }
                    }

                }

                System.out.println("Data sent!");

                System.out.println("Waiting time to search again ...");

            } catch (Exception ex) {
                System.err.println("Error: Failed to forward data " + host.getIp());
                ex.printStackTrace();

            }

            // Intervalo de espera
            try {
                Thread.sleep(host.getInterval() * 1000);
            } catch (InterruptedException ex) {
            }

        }

    }

}
