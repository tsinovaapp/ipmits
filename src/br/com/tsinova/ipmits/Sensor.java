package br.com.tsinova.ipmits;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONObject;

public class Sensor {

    private static final String PREFIX = "sensor_";
    private static final String TYPE = "Sensor";
    private static final List<String> SENSORS_TYPE;

    static {
        SENSORS_TYPE = new ArrayList<>();
        SENSORS_TYPE.add("Temperature");
        SENSORS_TYPE.add("Current");
        SENSORS_TYPE.add("Physical Security");
        SENSORS_TYPE.add("Processor");
        SENSORS_TYPE.add("Power Unit");
        SENSORS_TYPE.add("Other");
        SENSORS_TYPE.add("Drive Slot / Bay");
        SENSORS_TYPE.add("System Firmwares");
        SENSORS_TYPE.add("Watchdog1");
        SENSORS_TYPE.add("Critical Interrupt");
        SENSORS_TYPE.add("Module / Board");
        SENSORS_TYPE.add("Add-in Card");
        SENSORS_TYPE.add("Chip Set");
        SENSORS_TYPE.add("Cable / Interconnect");
        SENSORS_TYPE.add("System Boot Initiated");
        SENSORS_TYPE.add("OS Boot");
        SENSORS_TYPE.add("Watchdog2");
        SENSORS_TYPE.add("Entity Presence");
        SENSORS_TYPE.add("LAN");
        SENSORS_TYPE.add("Battery");
        SENSORS_TYPE.add("Version Change");
        SENSORS_TYPE.add("Voltage");
        SENSORS_TYPE.add("Fan");
        SENSORS_TYPE.add("Platform Security");
        SENSORS_TYPE.add("Power Supply");
        SENSORS_TYPE.add("Cooling Device");
        SENSORS_TYPE.add("Memory");
        SENSORS_TYPE.add("POST Memory Resize");
        SENSORS_TYPE.add("Event Logging Disabled");
        SENSORS_TYPE.add("System Event");
        SENSORS_TYPE.add("Button");
        SENSORS_TYPE.add("Microcontroller");
        SENSORS_TYPE.add("Chassis");
        SENSORS_TYPE.add("Other FRU");
        SENSORS_TYPE.add("Terminator");
        SENSORS_TYPE.add("Boot Error");
        SENSORS_TYPE.add("OS Critical Stop");
        SENSORS_TYPE.add("Platform Alert");
        SENSORS_TYPE.add("Monitor ASIC");
        SENSORS_TYPE.add("Management Subsys Health");
        SENSORS_TYPE.add("FRU State");
    }

    public static JSONArray getSensors(String host, String user,
            String password, String passwordSudo, String keyServerSerial, String valueServerSerial) throws Exception {

        JSONArray json = new JSONArray();

        for (String sensorType : SENSORS_TYPE) {

            String[] cmd = {"/bin/bash", "-c", "echo " + passwordSudo + "| sudo ipmitool -I lanplus -H " + host + " -U " + user + " -P '" + password + "' sdr type '" + sensorType + "'"};

            Process p = Runtime.getRuntime().exec(cmd);

            if (!p.waitFor(10, TimeUnit.SECONDS)) {
                p.destroy();
                continue;
            }

            List<String> output;
            try {
                output = Util.convertInputStreamInListString(p.getInputStream());
                p.destroy();
            } catch (Exception ex) {
                p.destroy();
                continue;
            }

            for (String line : output) {

                try {

                    String array[] = line.split("\\|");

                    if (array.length < 4) {
                        continue;
                    }

                    String sensor = array[0].trim();

                    if (sensor.equalsIgnoreCase("UID")) {
                        continue;
                    }

                    String status = getStatus(array[2].trim());
                    String value = array.length == 4 ? "" : getValueFormatted(array[4].trim());

                    JSONObject jsonSensor = new JSONObject();
                    jsonSensor.put(PREFIX + "name", sensor);
                    jsonSensor.put(PREFIX + "type", sensorType);
                    jsonSensor.put(PREFIX + "status", status);
                    jsonSensor.put(PREFIX + "value_formatted", value);
                    jsonSensor.put(PREFIX + "value", extractNumberInValueFormatted(value));
                    jsonSensor.put("type", TYPE);
                    jsonSensor.put(keyServerSerial, valueServerSerial);
                    json.put(jsonSensor);

                } catch (Exception ex) {
                }

            }

        }

        return json;

    }

    private static String getStatus(String status) {
        switch (status) {
            case "ok":
                return "Normal";
            case "cr":
                return "Crítico";
            case "ca":
                return "Cuidado";
            case "ns":
                return "Desabilitado";
            default:
                return "";
        }
    }

    private static String getValueFormatted(String value) {

        if (value == null) {
            return "";
        }

        String newValue = value.trim();
        newValue = newValue.replace(" degrees C", "ºC");
        newValue = newValue.replace("degrees C", "ºC");
        newValue = newValue.replace("Disabled", "Desabilitado");
        newValue = newValue.replace(" percent, Transition to Running", "%");
        newValue = newValue.replace("percent, Transition to Running", "%");
        newValue = newValue.replace(" percent", "%");
        newValue = newValue.replace("percent", "%");
        newValue = newValue.replace(", Presence detected", "");
        newValue = newValue.replace("Device Present", "Dispositivo presente");
        newValue = newValue.replace(", Device Enabled", "");
        newValue = newValue.replace("Drive Present", "Drive presente");
        newValue = newValue.replace("Fully Redundant", "Totalmente redundante");
        newValue = newValue.replace("Presence Detected", "Presença detectada");

        return newValue.trim();

    }

    private static Object extractNumberInValueFormatted(String value){
        
        if (value == null){
            return JSONObject.NULL;
        }
        
        if (value.isEmpty()){
            return JSONObject.NULL;
        }
        
        if (value.replaceAll("[^0-9?!\\.]","") == null || value.replaceAll("[^0-9?!\\.]","").trim().isEmpty()){
            return JSONObject.NULL;
        }
        
        return Double.parseDouble(value.replaceAll("[^0-9?!\\.]","").trim());
        
    }        

}
