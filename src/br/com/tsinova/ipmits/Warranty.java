package br.com.tsinova.ipmits;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONObject;

public class Warranty {

    public static final String TYPE = "Warranty";
    public static final String PREFIX = "warranty_";

    /**
     * Retorna a garantia do servidor
     * @param server
     * @param host
     * @param keyServerSerial
     * @param valueServerSerial
     * @return 
     */
    public static JSONArray getWarrantyBySerialNumberServer(JSONObject server, String host,
            String keyServerSerial, String valueServerSerial) {

        JSONArray json = new JSONArray();
        
        if (server == null){
            return json;
        }
        
        try {
            
            String sn = server.getString(Server.PREFIX + "product_serial");
            DateFormat format = new SimpleDateFormat("MMM dd, yyyy");

            URL oracle = new URL("https://support.hpe.com/hpsc/wc/public/find?submitButton=Senden&rows[0].item.countryCode=BR&rows[0].item.serialNumber=" + sn);
            URLConnection yc = oracle.openConnection();

            String text = "";

            try (BufferedReader in = new BufferedReader(new InputStreamReader(
                    yc.getInputStream()))) {
                String inputLine;
                while ((inputLine = in.readLine()) != null) {
                    text += inputLine.trim();
                }
            }

            String regex = ".*<td id=\"generate_table_" + sn + "\".*<table(.*>)</table>.*";

            String content = text.replaceAll(regex, "$1");

            regex = ".*<tbody>(.*)</tbody>.*";

            content = content.replaceAll(regex, "$1");

            String lines[] = content.split("</tr>");

            regex = ".*<td.*>(.*)";

            for (String line : lines) {

                String columns[] = line.split("</td>");

                String status = columns[columns.length - 1].trim().replaceAll(regex, "$1");
                Date endDate = format.parse(columns[columns.length - 2].trim().replaceAll(regex, "$1"));
                Date startDate = format.parse(columns[columns.length - 3].trim().replaceAll(regex, "$1"));
                String typeService = columns[columns.length - 4].trim().replaceAll(regex, "$1");

                JSONObject jsonWarranty = new JSONObject();
                jsonWarranty.put(PREFIX + "type", typeService);
                jsonWarranty.put(PREFIX + "start_date", getDateFormatted(startDate) + "T00:00:00.000Z");
                jsonWarranty.put(PREFIX + "end_date", getDateFormatted(endDate) + "T23:59:59.999Z");
                jsonWarranty.put(PREFIX + "status", status);
                jsonWarranty.put("type", TYPE);
                jsonWarranty.put(keyServerSerial, valueServerSerial);
                jsonWarranty.put("id_doc", getIdDoc(valueServerSerial, typeService));                
                json.put(jsonWarranty);

            }

            return json;

        } catch (Exception ex) {
            return json;
        }

    }

    private static String getIdDoc(String serverSerial, String type) throws Exception {
        return "warranty_" + serverSerial + "_" + type;
    }

    private static String getDateFormatted(Date date) {
        DateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
        return formatDate.format(date);
    }

}
