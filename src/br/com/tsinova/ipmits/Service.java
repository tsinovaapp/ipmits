package br.com.tsinova.ipmits;

import java.io.FileInputStream;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

public class Service extends Thread{
    
    private final List<ReadSend> listReadSends;

    public Service() {
        listReadSends = new ArrayList<>();
    }        
            
    private List<Host> getListServers(JSONObject json) throws Exception{
        JSONArray serversJson = json.getJSONArray("servers");
        List<Host> listServers = new ArrayList<>();        
        for (int i = 0; i < serversJson.length(); i++) {            
            listServers.add(new Host(serversJson.getJSONObject(i)));            
        }       
        return listServers;        
    }
    
    private Beat getBeat(JSONObject json) throws Exception{
        JSONObject beatJSON = json.getJSONObject("beat");
        Beat beat = new Beat(beatJSON);
        return beat;
    }
    
    public List<Output> getListOutputs(JSONObject json) throws Exception{        
        JSONObject outputJSON = json.getJSONObject("output");
        List<Output> listOutputs = new ArrayList<>();        
        if (outputJSON.has("logstash")){
            OutputLogstash outputLogstash = new OutputLogstash(outputJSON);
            listOutputs.add(outputLogstash);
        }        
        return listOutputs;                
    }

    @Override
    public void run() {
        
         // Efetua a leitura dos parâmetros ...
         
        System.out.println("Reading parameters ...");
        
        List<Host> listServers;
        List<Output> listOutputs;
        Beat beat;
        
        try {
            JSONObject json = new JSONObject(new JSONTokener(new FileInputStream(Paths.get("ipmits.json").toFile())));
            
            listServers = getListServers(json);
            listOutputs = getListOutputs(json);  
            beat = getBeat(json);
            
            System.out.println("Successful reading");
            
        } catch (Exception ex) {
            System.err.println("Failed to read");
            ex.printStackTrace();
            return;
        
        }
        
        
        // inicia processos
        
        System.out.println("Starting reading and sending processes ...");
        
        for(Host server : listServers){            
            ReadSend readSend = new ReadSend(server, listOutputs, beat);
            readSend.start();
            listReadSends.add(readSend);                                
        }        
        
        System.out.println("Read and send processes successfully started");
        
        
    }
    
    public static void main(String[] args) {        
        Service service = new Service();
        service.start();                
    }
    
}
