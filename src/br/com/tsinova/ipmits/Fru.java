package br.com.tsinova.ipmits;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Fru {

    public static List<List<String>> getFruByIPMItool(String host, String user, 
            String password, String passwordSudo) throws Exception {

        List<List<String>> output = new ArrayList<>();
        
        String[] cmd = {"/bin/bash", "-c", "echo " + passwordSudo + "| sudo ipmitool -I lanplus -H " + host + " -U " + user + " -P '" + password + "' fru "};

        Process p = Runtime.getRuntime().exec(cmd);

        if (!p.waitFor(10, TimeUnit.SECONDS)) {
            p.destroy();
            return output;
        }        
        
        try (BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()))) {
            String lineOut;        
            List<String> list = new ArrayList<>();
            while ((lineOut = input.readLine()) != null) {
                if (lineOut.trim().isEmpty()){
                    output.add(list);
                    list = new ArrayList<>();
                    continue;
                }
                list.add(lineOut);
            }
        }
        
        p.destroy();
        
        return output;
        
    }

}
