package br.com.tsinova.ipmits;

import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class Memory {

    private static final String REQUIRED_ATTRIBUTES[] = new String[]{"memory_type", "memory_size"};
    private static final String PREFIX = "memory_";
    private static final String TYPE = "Memory";

    /**
     * Coleta as informações de memórias
     *
     * @param output
     * @param host
     * @param keyServerSerial
     * @param valueServerSerial
     * @return
     */
    public static JSONArray getMemorysByFru(List<List<String>> output, String host, 
            String keyServerSerial, String valueServerSerial) {

        JSONArray jsonArray = new JSONArray();

        for (List<String> deviceInfo : output) {

            boolean deviceIsMemory;

            try {
                deviceIsMemory = Util.contaisOneKeysInDeviceInfo(deviceInfo, REQUIRED_ATTRIBUTES);
            } catch (Exception ex) {
                continue;
            }

            if (!deviceIsMemory) {
                continue;
            }

            try {
                
                JSONObject jsonMemoryInfo = Util.convertListOutputToJSONObject(deviceInfo, PREFIX);
                jsonMemoryInfo.put("type", TYPE);
                jsonMemoryInfo.put(keyServerSerial, valueServerSerial);
                jsonMemoryInfo.put(PREFIX + "status", "Atual");
                jsonMemoryInfo.put("id_doc", getIdDoc(jsonMemoryInfo, new String[]{"serial_number"}, host, "fru_device_description"));                    
                jsonArray.put(jsonMemoryInfo);
                
            } catch (Exception ex) {
            }

        }

        return jsonArray;

    }

    private static String getIdDoc(JSONObject json, String keys[], String host, String k) throws Exception {
        for (String key : keys) {
            if (!json.has(PREFIX + key)) {
                continue;
            }
            return "memory_" + json.getString(PREFIX + key);
        }
        return "memory_" + host + "_" + json.getString(PREFIX + k);
    }

}
