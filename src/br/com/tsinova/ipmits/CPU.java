package br.com.tsinova.ipmits;

import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class CPU {

    private static final String REQUIRED_VALUES[] = new String[]{".*CPU.*\\d+GHz.*", ".*cpu.*\\d+GHz.*", ".*CPU.*\\d+MHz.*"};
    private static final String PREFIX = "cpu_";
    private static final String TYPE = "CPU";

    /**
     * Coleta as informações de cpus
     *
     * @param output
     * @param host
     * @param keyServerSerial
     * @param valueServerSerial
     * @return
     * @throws Exception
     */
    public static JSONArray getCPUsByFru(List<List<String>> output, String host, 
            String keyServerSerial, String valueServerSerial) throws Exception {

        JSONArray jsonArray = new JSONArray();

        for (List<String> deviceInfo : output) {

            boolean deviceIsCPU;

            try {
                deviceIsCPU = Util.contaisOneValueInDeviceInfo(deviceInfo, REQUIRED_VALUES);
            } catch (Exception ex) {
                continue;
            }

            if (!deviceIsCPU) {
                continue;
            }

            try {
                
                JSONObject jsonCPUInfo = Util.convertListOutputToJSONObject(deviceInfo, PREFIX);
                jsonCPUInfo.put("type", TYPE);
                jsonCPUInfo.put(keyServerSerial, valueServerSerial);
                jsonCPUInfo.put(PREFIX + "status", "Atual");                
                jsonCPUInfo.put("id_doc", "cpu_" + valueServerSerial + "_" + jsonCPUInfo.getString(PREFIX + "fru_device_description"));

                //jsonCPUInfo.put("id_doc", getIdDoc(jsonCPUInfo, new String[]{"serial_number"}, host, "fru_device_description"));
                jsonArray.put(jsonCPUInfo);
            } catch (Exception ex) {
            }

        }

        return jsonArray;

    }

//    private static String getIdDoc(JSONObject json, String keys[], String host, String k) throws Exception {
//        for (String key : keys) {
//            if (!json.has(PREFIX + key)) {
//                continue;
//            }
//            return "cpu_" + json.getString(PREFIX + key);
//        }
//        return "cpu_" + host + "_" + json.getString(PREFIX + k);
//    }

}
